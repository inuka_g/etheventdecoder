This an ethereum event decoder which decodes events and funciton from an ethereum block chain or decodes event funtion calls captured in a chrome web log (web3js calls from a DAPP UI)

Arguments are passed in the settings.json file

```
settings:
{
  "abipath": "C:\\dlt2\\hashlynx",
  "logfile": "logfile.log",
  "rpcurl": "HTTP://127.0.0.1:7545",
  "startblock": "",
  "endblock": "",
  "address": {    
  }
}
```


- chromelog: Optional, path to a chrome web log.
- abipath: Required, this points to the abi files of the contract we will decode. Supports both .json or .abi files
- logfile: Optional, outputs the decoded output to the this logfile.
- rpcurl: RCP url of the etherum node.
- startblock: Optional, this is the start when decoding begins, the default is 0.
- endblock: Optional, specifies the end block to decode. When this specifies, the program will not continously poll after the decode completes.
- address: Optional. An array of addresses. When specified will only show decode for transactions from these addresses only


How it works:
1. Uses Netherium to load abi files.
2. Loads functions and events and builds a dictionary of sha3 hashes.
3. Waits for each block from the block chain.
4. When a block is recieved. Looks in the logs for matching events. If a match is found the decode is shown.
5. Searches logs for tractions. Searches the transactions for matching hashes, if found the decode is shown.
6. In case a chrome log is used, the program loads and parses the chrome log web3js calls and decodes it using the abi files which are provided


Limitations:
If there are contracts with matching event or function signatures it does not distinguish between the two.

Improvements:
Instead of going from the logs to the transactions, rewrite to go from transactions to the logs. Probalby more efficient that way.
Address matching is untested
