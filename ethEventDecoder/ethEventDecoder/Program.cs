﻿using System;
using Nethereum.ABI.FunctionEncoding.Attributes;
using Nethereum.RPC.Eth.DTOs;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.FileExtensions;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.Configuration.Binder;

namespace ethEventDecoder
{
    class Program
    {

        /*
         * -p path
         * -c connection string
         * -s startblock   optional
         * -e endblock     optional
         * -l logfile      optional
         */
         /*
          * Current is connectionstring ABIPTH logfile startblock endblock
          */

        Dictionary<string, string> commandArgs = new Dictionary<string, string>();
        static void Main(string[] args)
        {
            //string path = args[0];
            //string startblock = "";
            //string endblock = "";
            //if (args.Length>1)
            //    startblock = args[1];
            //if (args.Length > 2)
            //    endblock = args[2];          
            

            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("settings.json", true, true)
                .Build();
            string something = config["funny"];

            string web3connection = config["rpcurl"];//@"HTTP://127.0.0.1:7545";
            string path = config["abipath"];//@"C:\dlt2\lenderDAOStable\contracts\build\contracts";
            string logfile = config["logfile"];
            uint start = 0;
            uint end = 0;          
            if (config["startblock"] != null && !String.IsNullOrEmpty(config["startblock"]))
                start = (uint) Int32.Parse(config["startblock"]);
            if (config["endblock"] !=null && !String.IsNullOrEmpty(config["endblock"]))
                end = (uint)Int32.Parse(config["endblock"]);
            if(!Directory.Exists(path))
            {
                Console.WriteLine("Cannot acess path, check if it exists or if the permission are correct " + path);
                return;
            }
            var addrlist = config.GetSection("address:list").Get<List<string>>();            

            Console.WriteLine("ABI Path is " + path);
            Console.WriteLine("Connection string is " + web3connection);
            Console.WriteLine("Logfile is " + logfile);
            EventDecoder decoder = null;
            try
            {
                 decoder = new EventDecoder(path, logfile, web3connection, start, end);
            }
            catch(Exception exp)
            {
                Console.WriteLine("Error connecting to RPC " + exp.Message);
                return;
            }

            if (addrlist != null && addrlist.Count > 0)
            {
                Console.WriteLine("address filtering on");
                decoder.AddAddress(addrlist);
                
            }
            if (config["chromelog"] != null && !String.IsNullOrEmpty(config["chromelog"]))
            {
                Console.WriteLine("Decoding Chromelog");
                chromeLog clog = new chromeLog(config["chromelog"]);
                decoder.DecodeChromeLog(clog);
            }
            else
            {
                Console.WriteLine("Decoding started press any key to exit");
                decoder.StartDecode();
                Console.ReadKey();
                decoder.Stop();
            }
        }


       
    }

}
