﻿using System;
using Nethereum.ABI.FunctionEncoding;
using Nethereum.ABI.JsonDeserialisation;
using Nethereum.ABI.Model;
using Nethereum.Web3;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Threading.Tasks;
using Nethereum.Hex.HexTypes;
using System.Threading;

namespace ethEventDecoder
{
    public class EventDecoder
    {
        Dictionary<string, EventABI> eventHashStore = new Dictionary<string, EventABI>();
        Dictionary<string, FunctionABI> functionHashStore = new Dictionary<string, FunctionABI>();
        List<string> addrlist;
        bool addressfiltering = false;
        List<ContractABI> contractList;
        string logfile;
        HexBigInteger oldBlock;
        HexBigInteger currentBlock;
        Task decodedThread = null;
        Web3 connection;
        bool stop = false;
        StreamWriter writer;
        bool nopoll=false;

        public EventDecoder(string path, string _logfile, string endpoint, uint start, uint end)
        {
            contractList = LoadContracts(path);
            foreach(ContractABI cont in contractList)
            {
                foreach(EventABI evt in cont.Events)
                {
                    if(!eventHashStore.ContainsKey("0x" + evt.Sha3Signature))
                        eventHashStore.Add("0x" + evt.Sha3Signature, evt);
                }
                foreach(FunctionABI fn in cont.Functions)
                {
                    if(!functionHashStore.ContainsKey("0x" + fn.Sha3Signature))
                        functionHashStore.Add("0x" + fn.Sha3Signature, fn);
                }
            }            
            logfile = _logfile;
            if (!String.IsNullOrEmpty(logfile))
                writer = new StreamWriter(logfile);
            connection = new Web3(endpoint);
            //this a sanity check to see if the connection is ok
            Task t =  connection.Eth.Blocks.GetBlockNumber.SendRequestAsync();
            t.Wait();
            oldBlock = new HexBigInteger(start);
            if (end > 0)
            {
                currentBlock = new HexBigInteger(end);
                nopoll = true;
            }


        }

        public void StartDecode()
        {
            if (decodedThread == null)
                decodedThread = DecodedLoop();
        }

        public void Stop()
        {
            stop = true;
            writer.Close();
            decodedThread.Wait();

            decodedThread = null;
        }

        private void OutputLine(string line)
        {
            Console.WriteLine(line);
            if (writer != null)
                writer.WriteLine(DateTime.Now+" "+ line);
            writer.Flush();
        }


        public  void DecodeChromeLog(chromeLog clog)
        {
            FunctionCallDecoder dec = new FunctionCallDecoder();
            foreach (logParam log in clog.Payloads)
            {
                if(!log.isCall)
                {
                    OutputLine(log.methodName);
                }
                else
                {
                    string hash = log.data.Substring(0, 10);
                    //0x3593c905
                    // 0xfdacd576
                    if (functionHashStore.ContainsKey(hash))
                    {
                        var fnABI = functionHashStore[hash];
                        var res = dec.DecodeFunctionInput(hash, log.data, fnABI.InputParameters);
                        string inputdecoded = "";
                        foreach (var ot in res)
                        {
                            inputdecoded += String.Format("{0}: {1}: {2} ,", ot.Parameter.Name, ot.Parameter.Type, ot.Result);
                        }
                        string output =  "";
                        if(!String.IsNullOrEmpty(log.result))
                        {
                            try
                            {
                                List<ParameterOutput> opParams = new List<ParameterOutput>();
                                foreach (var p in fnABI.OutputParameters)
                                {
                                    opParams.Add(new ParameterOutput { Parameter = p });
                                }
                                var retvals = dec.DecodeDefaultData(log.result, fnABI.OutputParameters);
                                foreach (var ip in retvals)
                                {
                                    output += String.Format("{0}: {1}: {2} ,", ip.Parameter.Name, ip.Parameter.Type, ip.Result);
                                }
                                if (!String.IsNullOrEmpty(output))
                                {
                                    output = "RETURN VAL: " + output;
                                }
                            }
                            catch
                            {
                                Console.WriteLine("Error for {0} error is {1} ",fnABI.Name, output);
                            }
                        }

                        OutputLine(String.Format("FUNCTION {0} {1} {2}  ", fnABI.Name, inputdecoded,output));

                    }
                   
                }
            }
        }


        private async Task DecodedLoop()
        {
            //oldBlock = await connection.Eth.Blocks.GetBlockNumber.SendRequestAsync();
            
            EventTopicDecoder decoder = new EventTopicDecoder();
            FunctionCallDecoder dec = new FunctionCallDecoder();
            HexBigInteger lastBlockNumber = new HexBigInteger(0);
            while (!stop )
            {
                if (!nopoll)
                {
                    Thread.Sleep(1000);
                    currentBlock = await connection.Eth.Blocks.GetBlockNumber.SendRequestAsync();
                }
                Nethereum.RPC.Eth.DTOs.NewFilterInput filter;
                if (addressfiltering)
                {
                    filter = new Nethereum.RPC.Eth.DTOs.NewFilterInput
                    {
                        FromBlock = new Nethereum.RPC.Eth.DTOs.BlockParameter(oldBlock),
                        ToBlock = new Nethereum.RPC.Eth.DTOs.BlockParameter(currentBlock),
                        Address = addrlist.ToArray()
                    };
                }
                else
                {
                    filter = new Nethereum.RPC.Eth.DTOs.NewFilterInput
                    {
                        FromBlock = new Nethereum.RPC.Eth.DTOs.BlockParameter(oldBlock),
                        ToBlock = new Nethereum.RPC.Eth.DTOs.BlockParameter(currentBlock),
                    };
                    Console.WriteLine("Reading blocks " + oldBlock + " to " + currentBlock);
                }

                var loglist = await connection.Eth.Filters.GetLogs.SendRequestAsync(filter);      
                
                foreach (var log in loglist)
                {
                    if (!String.IsNullOrEmpty(log.TransactionHash) && lastBlockNumber !=log.BlockNumber)
                    {
                        lastBlockNumber = log.BlockNumber;
                        var tr = connection.Eth.Blocks.GetBlockWithTransactionsByNumber.SendRequestAsync(new Nethereum.RPC.Eth.DTOs.BlockParameter(log.BlockNumber));
                        //var tr= connection.Eth.Blocks.GetBlockWithTransactionsByHash.SendRequestAsync(log.TransactionHash);
                        tr.Wait();
                        if (tr.Result.Transactions.Length > 0)
                        {
                            
                            foreach (var t in tr.Result.Transactions)
                            {
                                
                                string hash = t.Input.Substring(0, 10);
                                //0x3593c905
                                // 0xfdacd576
                                if (functionHashStore.ContainsKey(hash))
                                {
                                    var fnABI = functionHashStore[hash];
                                    var res = dec.DecodeFunctionInput(hash, t.Input, fnABI.InputParameters);
                                    var res2 = dec.DecodeOutput(t.Input);
                                    var res3 = dec.DecodeOutput(t.Value);
                                    string inputdecoded = "";
                                    foreach (var ot in res)
                                    {
                                        inputdecoded += String.Format("{0}: {1}: {2} ,", ot.Parameter.Name, ot.Parameter.Type, ot.Result);
                                    }
                                    OutputLine(String.Format("{4} FUNCTION {0} ({1},{2})  ( {3}  )", fnABI.Name, t.From,t.To, inputdecoded, t.BlockNumber));
                                    

                                }
                                /*  if ((t.From == "0x448a5065aebb8e423f0896e6c5d525c040f59af3" || t.To == "0x448a5065aebb8e423f0896e6c5d525c040f59af3"))
                                  {
                                      Console.WriteLine("address match found");
                                  }
                                  if (t.TransactionHash.ToLower().StartsWith("0x" + bitefn.Sha3Signature.ToLower()))
                                  {
                                      dec.DecodeFunctionInput(bitefn.Sha3Signature, t.Input);
                                  }
                                  if (t.Input.ToLower().StartsWith("0x" + bitefn.Sha3Signature.ToLower()))
                                  {
                                      var res = dec.DecodeFunctionInput("0x" + bitefn.Sha3Signature, t.Input, bitefn.InputParameters);
                                      var res2 = dec.DecodeOutput(t.Input);
                                      var res3 = dec.DecodeOutput(t.Value);
                                      blockNum = i;

                                  }
                              }*/
                            }
                        }
                    }
                    //Console.WriteLine("Decoding block " + log.BlockNumber);
                    if (log.Topics.Length > 0 )
                    {
                        
                        if (eventHashStore.ContainsKey(log.Topics[0].ToString()))
                        {
                            var evt = eventHashStore[log.Topics[0].ToString()];
                            var data = decoder.DecodeDefaultTopics(evt, log.Topics, log.Data);
                            String logmsg = log.BlockNumber +" EVENT: " + evt.Name;
                            foreach (var param in data)
                            {
                                logmsg +=  " " + param.Parameter.Name + " = " + param.Result;

                                //var event_data = data.GetType().GetProperty("Event").GetValue(data, null);
                                //String logmsg = "EVENT: " + evt.ABI.Name;
                                //foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(event_data))
                                //{
                                //    string name = descriptor.Name;
                                //    var value = descriptor.GetValue(event_data);
                                //    logmsg += String.Format(" {0}={1}", name, value);
                                //}
                                //Trace.WriteLine(logmsg);
                            }
                            OutputLine(logmsg);                          
                        }
                    }
                    
                   


                }
                if (nopoll)
                    return;
                oldBlock = new  HexBigInteger((BigInteger)currentBlock+1);


            }

        }

        public void AddAddress(List<string> addrlist)
        {
            foreach(string addr in addrlist)
            {
                addrlist.Add(addr);
            }
            addressfiltering = true;


        }

        static List<ContractABI> LoadContracts(string path)
        {
            ABIDeserialiser ab = new ABIDeserialiser();
            //StreamReader reader = new StreamReader(@"C:\dlt2\lenderDAO\contracts\build\contracts\BaseLoanContract.json");
            //StreamReader reader = new StreamReader(@"C:\dlt2\lenderDAO\contracts\bin\contracts\BaseLoanContract.abi");
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            //C:\dlt2\lenderDAOStable\contracts\bin\contracts
            //DirectoryInfo directoryInfo = new DirectoryInfo(@"C:\dlt2\lenderDAOStable\contracts\bin\contracts");
            FileInfo[] files1 = directoryInfo.GetFiles("*.json, *.abi", SearchOption.AllDirectories);
            FileInfo[] files2 = directoryInfo.GetFiles("*.abi", SearchOption.AllDirectories);
            List<FileInfo> abiFiles = new List<FileInfo>();
            abiFiles.AddRange(files1);
            abiFiles.AddRange(files2);
            List<ContractABI> contractslst = new List<ContractABI>();
            foreach (var fl in abiFiles)
            {
                using (StreamReader reader = new StreamReader(fl.FullName))
                {
                    //1.
                    string filestring = reader.ReadToEnd();
                    if (fl.Extension.Equals(".abi"))
                    {

                        try
                        {
                            contractslst.Add(ab.DeserialiseContract(filestring));
                        }
                        catch
                        {
                            Console.WriteLine("Error not able to read file " + fl);
                        }
                    }
                    else
                    {

                        //2.
                        // Grab the abi array from the JSON file, it is possible the format maybe different
                        // and the whole file is abi
                        try
                        {

                            JObject obj = (JObject)JsonConvert.DeserializeObject(filestring);
                            JToken tok = obj["abi"];
                            string content = tok.ToString();
                            contractslst.Add(ab.DeserialiseContract((JArray)tok));
                        }
                        catch
                        {
                            Console.WriteLine("failed to load file " + fl);
                        }
                    }
                }

            }
            return contractslst;
        }
    }

}
